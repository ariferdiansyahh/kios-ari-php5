﻿# Host: localhost  (Version 5.5.5-10.1.36-MariaDB)
# Date: 2019-07-13 11:35:57
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "admin"
#

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) NOT NULL,
  `pass` varchar(70) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

#
# Data for table "admin"
#

INSERT INTO `admin` VALUES (9,'admin','21232f297a57a5a743894a0e4a801fc3','Koala.jpg'),(10,'ari','fc292bd7df071858c2d0f955545673c1','Penguins.jpg');

#
# Structure for table "barang"
#

DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text NOT NULL,
  `jenis` text NOT NULL,
  `suplier` text NOT NULL,
  `modal` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

#
# Data for table "barang"
#

INSERT INTO `barang` VALUES (37,'mie goreng','makanan','mie goreng',2000,3000,30,100),(38,'mie rebus','makanan','mie rebus',2000,3000,30,50),(39,'ale ale','minuman','ale ale',500,1000,57,70);

#
# Structure for table "barang_laku"
#

DROP TABLE IF EXISTS `barang_laku`;
CREATE TABLE `barang_laku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `nama` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total_harga` int(20) NOT NULL,
  `laba` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

#
# Data for table "barang_laku"
#

INSERT INTO `barang_laku` VALUES (68,'2019-07-12','mie goreng',50,3000,150000,50000),(69,'2019-07-12','mie goreng',20,3000,60000,20000),(70,'2019-07-12','mie rebus',20,3000,60000,20000),(71,'2019-07-12','ale ale',13,1000,13000,6500);

#
# Structure for table "pengeluaran"
#

DROP TABLE IF EXISTS `pengeluaran`;
CREATE TABLE `pengeluaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `keperluan` text NOT NULL,
  `nama` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "pengeluaran"
#

INSERT INTO `pengeluaran` VALUES (1,'2015-02-06','de','diki',1234);
